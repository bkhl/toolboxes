#!/bin/bash

set -xueo pipefail

dnf -y install gmp ncurses-compat-libs
dnf clean all
rm -rf /var/cache/dnf
