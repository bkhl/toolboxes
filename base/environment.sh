#!/bin/bash

export TOOLBOX_DIR="$PWD"

# Pipx installation locations
export PIPX_HOME=/usr/local/pipx/venvs
export PIPX_BIN_DIR=/usr/local/bin

if [[ -f .toolboxenv ]]; then
    set -o allexport
    # shellcheck disable=SC1091
    source .toolboxenv
    set +o allexport
fi

cd() {
    # shellcheck disable=SC2164

    if (( $# )); then
        builtin cd "$@" || return
    else
        builtin cd "$TOOLBOX_DIR" || return
    fi
}

# Revert the notification that Toolbox has made to the terminal emulator that
# we are in a container, to prevent new windows to open in the same container.
#
# See https://github.com/containers/toolbox/issues/218
printf "\033]777;container;pop;;\033\\"
