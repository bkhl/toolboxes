#!/bin/bash

set -xueo pipefail

# Go to build directory
cd /build

# Install additional RPM packages
dnf -y update

dnf -y install \
    @development-tools \
    @c-development \
    abattis-cantarell-fonts \
    bc \
    bind-utils \
    breezy \
    dos2unix \
    emacs \
    file \
    glibc-all-langpacks \
    jq \
    mmv \
    moreutils \
    mosh \
    pipx \
    python-unversioned-command \
    sha \
    ShellCheck \
    sshpass \
    subversion \
    tmux \
    wl-clipboard \
    xmlstarlet

dnf clean all
rm -rf /var/cache/dnf

# Install git trim
curl --proto '=https' --tlsv1.2 --silent --show-error --fail \
    --location 'https://github.com/foriequal0/git-trim/releases/download/v0.4.2/git-trim-linux-v0.4.2.tgz' \
    | tar xzf - -C /usr/local/bin/ --strip-components=1 git-trim/git-trim

# Install some programs with pipx
pipx install termtosvg
pipx install yamllint

# Install AWS CLI client
curl --proto '=https' --tlsv1.2 --silent --show-error --fail \
    --location 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' \
    --output awscli-exe-linux-x86_64.zip

unzip -q awscli-exe-linux-x86_64.zip

aws/install

tee /etc/bash_completion.d/aws << HERE
complete -C /usr/local/bin/aws_completer aws
HERE
