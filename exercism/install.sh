#!/bin/bash

set -xueo pipefail

mkdir /exercism

curl --silent --show-error \
    --location 'https://github.com/exercism/cli/releases/download/v3.0.13/exercism-linux-64bit.tgz' \
    | tar xzf - -C /exercism/

/exercism/exercism upgrade
