#!/bin/bash

set -xueo pipefail

dnf -y install python3-plumbum tidy xmlstarlet
dnf clean all
rm -rf /var/cache/dnf

curl --silent --show-error \
    --location 'https://github.com/getzola/zola/releases/download/v0.13.0/zola-v0.13.0-x86_64-unknown-linux-gnu.tar.gz' \
    | tar xzf - -C /usr/local/bin/
