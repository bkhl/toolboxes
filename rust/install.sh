#!/bin/bash

set -xueo pipefail

curl --proto '=https' --tlsv1.2 --silent --show-error --fail --location 'https://sh.rustup.rs' > /rustup-init

sh /rustup-init -y --no-modify-path
rm /rustup-init

chown -R 1000:1000 "$RUSTUP_HOME"
chown -R 1000:1000 "$CARGO_HOME"

mkdir -p "$CCACHE_DIR"
chown -R 1000:1000 "$CCACHE_DIR"
