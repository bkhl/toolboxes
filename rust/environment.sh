#!/bin/bash

export RUSTUP_HOME=/opt/rustup
export CARGO_HOME=/opt/cargo
export CCACHE_DIR=/var/cache/ccache_rust

# Change ownership of directories if not the current user.
for directory in "$RUSTUP_HOME" "$CARGO_HOME" "$CCACHE_DIR"; do
    if [[ $(stat -c '%u' $directory) != $(id -u) ]]; then
        sudo chown -R "$(id -u):$(id -g)" $directory
    fi
done

# shellcheck disable=SC1091
source /opt/cargo/env
