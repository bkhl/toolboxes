#!/bin/bash

set -xueo pipefail

# Go to build directory
cd /build

# Install dependencies for Pyenv
dnf install -y \
    @development-tools \
    bzip2 \
    bzip2-devel \
    findutils \
    libffi-devel \
    openssl-devel \
    readline-devel \
    sqlite \
    sqlite-devel \
    xz \
    xz-devel \
    zlib-devel
dnf clean all
rm -rf /var/cache/dnf

# Install Pyenv
curl --proto '=https' --tlsv1.2 --silent --show-error --fail --remote-name \
    --location 'https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer'
HOME=/build bash pyenv-installer
PATH="${PYENV_ROOT}/bin${PATH:+:$PATH}"
eval "$(pyenv init -)"

# Install Python with Pyenv
for python_version in 2.7.18 3.8.6; do
    pyenv install --verbose "$python_version"
    PYENV_VERSION="$python_version" pyenv exec pip install --no-cache-dir --upgrade pip
done

# Activate both Python 2 and 3 globally.
pyenv global 3.8.6 2.7.18

# Install Poetry
curl --proto '=https' --tlsv1.2 --silent --show-error --fail --remote-name \
    --location 'https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py'
HOME=/build /usr/bin/python get-poetry.py --yes

# Install Black using pipx
pipx install black

# Ensure that directories for Python environment exists and are writable by the
# user.
for directory in "$PYENV_ROOT" "$WORKON_HOME" "$POETRY_HOME" "$POETRY_CACHE_DIR"; do
    mkdir -p "$directory"
    chown -R 1000:1000 "$directory"
done
