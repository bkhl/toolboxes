#!/bin/bash

# Virtualenv location
export WORKON_HOME=/var/cache/virtualenvs

# Load Pyenv. Skips loading if it's already in path, to prevent clobbering the
# path in case we spawn a shell after Pyenv is already loaded.
if [[ :${PATH} != *:/opt/pyenv/* ]]; then
    export PYENV_ROOT=/opt/pyenv
    export PYENV_VIRTUALENV_CACHE_PATH="$WORKON_HOME"
    PATH="${PYENV_ROOT}/bin${PATH:+:$PATH}"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# Poetry
export POETRY_HOME=/opt/poetry

# shellcheck source=/dev/null
source "$POETRY_HOME/env"

export POETRY_CACHE_DIR=/var/cache/poetry
export POETRY_VIRTUALENVS_PATH="$WORKON_HOME"

# Change ownership of cache directories if not the current user.
for directory in "$PYENV_ROOT" "$WORKON_HOME" "$POETRY_HOME" "$POETRY_CACHE_DIR"; do
    if [[ $(stat -c '%u' $directory) != $(id -u) ]]; then
        sudo chown -R "$(id -u):$(id -g)" "$directory"
    fi
done
